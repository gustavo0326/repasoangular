import { Component } from '@angular/core';
import { User } from './models/User';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  themeFromParent: string = "red";
  usr: User;

  ngOnInit(){
    this.usr = new User("Pablo","algo","1234",4);
    console.log(this.usr.saludar());
    this.usr.mapeame();
  }

  interceptar($event) {
    console.log("Evento interceptado por app component, dice: "+$event);
    this.themeFromParent = $event;
  }
}


