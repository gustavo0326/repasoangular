import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemeToolComponent } from './theme-tool.component';

describe('ThemeToolComponent', () => {
  let component: ThemeToolComponent;
  let fixture: ComponentFixture<ThemeToolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThemeToolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeToolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
