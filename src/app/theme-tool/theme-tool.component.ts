import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-theme-tool',
  templateUrl: './theme-tool.component.html',
  styleUrls: ['./theme-tool.component.scss']
})
export class ThemeToolComponent implements OnInit {

  @Output() emisor = new EventEmitter<string>();

  theme:string = "red";

  constructor() { }

  ngOnInit() {
  }

  emitirEvento() {
    this.theme = (this.theme == "red")? "blue" : "red";
    console.log("Emitiendo cambio de theme");
    this.emisor.emit(this.theme);
  }

}
