export class User{

    constructor(private name: string, private email: string, private password: string, private age: number){}

    saludar(){
        return `Hola mi nombre es ${this.name}`;
    }

    mapeame(){
        console.log(this);
        Object.keys(this).map( (key, index) => console.log(`${key}: ${index}, value: ${this[key]}`) );
    }
}